import gitlab
import csv

# private token or personal token authentication
gl = gitlab.Gitlab.from_config('TPO', ['gl.cfg'])

# use the username/password authentication.
gl.auth()

# Core's tor project
project_id = 426
project = gl.projects.get(project_id)

# csv for output
# closed date, created username, assignee user name

# loop through all pages in the project
page_number = 1 #1 #45
total_pages = 45 #45 #97

with open('closed_tor_issues1-45.csv', 'w', newline='') as csvfile:
    fieldnames = ['id', 'closed at', 'author', 'assignee']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()

    for p in range(page_number,total_pages):
      closed_issues = project.issues.list(state='closed', scope='all',per_page=100, page_number=p)
      for c in closed_issues:
          # 'assignee', 'assignees', 'attributes', 'author', 'awardemojis', 'closed_at', 'closed_by', 'confidential', 'created_at', 'delete', 'description', 'discussion_locked', 'discussions', 'downvotes', 'due_date', 'get_id', 'has_tasks', 'id', 'iid', 'issue_type', 'labels', 'links', 'manager', 'merge_requests_count', 'milestone', 'move', 'moved_to_id', 'notes', 'participants', 'project_id', 'references', 'related_merge_requests', 'reset_spent_time', 'reset_time_estimate', 'resourcelabelevents', 'resourcemilestoneevents', 'resourcestateevents', 'save', 'service_desk_reply_to', 'state', 'subscribe', 'task_completion_status', 'time_estimate', 'time_stats', 'title', 'todo', 'type', 'unsubscribe', 'updated_at', 'upvotes', 'user_agent_detail', 'user_notes_count', 'web_url']
          aut = "None"
          ass = "None"
          if c.author != None:
              aut = c.author['username']
          if c.assignee != None:
              ass = c.assignee['username']
          writer.writerow({'id': c.id , 'closed at': c.closed_at, 'author': aut, 'assignee': ass})
